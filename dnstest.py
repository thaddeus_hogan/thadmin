#!/usr/bin/env python

import dns.query
import dns.zone
import dns.tsigkeyring
from dns import rdatatype

from thadmin.settings import ZM_DEFAULT_NS, ZM_DEFAULT_KEY, ZM_DEFAULT_KEY_NAME, ZM_DEFAULT_KEY_ALGO

print "XFER of thogan.lan ..."

keyring = dns.tsigkeyring.from_text({
    ZM_DEFAULT_KEY_NAME: ZM_DEFAULT_KEY
})

xfr = dns.query.xfr(ZM_DEFAULT_NS, 'thogan.lan', keyring = keyring, keyname = ZM_DEFAULT_KEY_NAME, keyalgorithm = ZM_DEFAULT_KEY_ALGO)
zone = dns.zone.from_xfr(xfr)
names = zone.nodes.keys()
names.sort()
for n in names:
    print unicode(n)
    for rd in zone[n].rdatasets:
        if rd.rdtype == rdatatype.A:
            for i in rd.items:
                print i.address