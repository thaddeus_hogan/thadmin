function AuthController($scope, $rootScope, $http, $location) {
    $scope.AS_NONE = 0;
    $scope.AS_WORKING = 1;
    $scope.AS_FAIL = 2;

    $scope.credentials = {
        email: "",
        password: ""
    };

    $scope.auth_state = $scope.AS_NONE;

    $scope.do_login = function() {
        $scope.auth_state = $scope.AS_WORKING;
        var spinner_box = document.getElementById('login_spinner');
        $scope.spinner = new Spinner(spinner_config).spin(spinner_box);

        $http.post('/common/auth/login', JSON.stringify($scope.credentials))
            .success(function(data, status, headers, config) {
                $scope.spinner.stop();
                $rootScope.authenticated = true;
                $rootScope.userinfo = data;
                $location.path('/index');
            })
            .error(function(data, status, headers, config) {
                $scope.spinner.stop();
                $scope.auth_state = $scope.AS_FAIL;
                $scope.credentials.password = "";
            });
    };
}