function ZoneListController($scope, $http, $rootScope, $location) {
    $scope.zonelist = [];

    $scope.load_zones = function() {
        var spinner_box = document.getElementById('spinner_box');
        $scope.spinner = new Spinner(spinner_config).spin(spinner_box);
        $scope.loading = true;

        $http.get('/zones/')
            .success(function(data, status, headers, config) {
                $scope.zonelist = data;
                $scope.spinner.stop();
                $scope.loading = false;
            });
    };

    $scope.del_zone = function(zone) {
        console.log("Asked to DELETE " + zone.zone_name);
    };

    $scope.load_zones();
}

function ZoneAddController($scope, $http, $rootScope, $location) {
    $scope.form_errors = 0;
    $scope.saving = false;
    $scope.zone = {
        tsig_key_algo: 'HMAC_SHA512'
    };

    $scope.add_zone = function() {
        $scope.saving = true;
        var spinner_box = document.getElementById('spinner_box');
        $scope.spinner = new Spinner(spinner_config).spin(spinner_box);

        $http.post('/zones/', $scope.zone)
            .success(function(data, status, headers, config) {
                $scope.spinner.stop();
                $location.path('/zoneman/zonelist');
            })
            .error(function(data, status, headers, config){
                $scope.saving = false;
                $scope.spinner.stop();
                $scope.form_errors = data;
            });
    };
}

function ZoneRecordsController($scope, $routeParams, $http, $rootScope, $location) {
    $scope.zone_name = $routeParams.zone_name;
    $scope.record_types = [ 'A', 'CNAME', 'MX', 'SRV', 'TXT', 'SPF' ];
    $scope.shown_forms = {};
    $scope.shown_delete_confirm = {};
    $scope.form_errors = 0;
    $scope.edited_record = {};
    $scope.scroll_return = 0;

    $scope.hide_forms = function() {
        for (rti in $scope.record_types) { $scope.shown_forms[$scope.record_types[rti]] = false }
        $scope.shown_delete_confirm = {};
        $scope.form_errors = 0;
    };

    $scope.load_records = function() {
        $scope.records = {};

        var spinner_box = document.getElementById('spinner_box');
        $scope.spinner = new Spinner(spinner_config).spin(spinner_box);
        $scope.loading = true;

        $http.get('/zones/' + $scope.zone_name + '/records')
            .success(function(data, status, headers, config) {
                $scope.spinner.stop();
                $scope.loading = false;
                $scope.records = data;

                if ($scope.scroll_return > 0) {
                    setTimeout(function() {
                        scroll_set($scope.scroll_return);
                        $scope.scroll_return = 0;
                    }, 0);
                }
            });
    };

    $scope.edit_record = function(rt, rec, replace) {
        $scope.hide_forms();

        $scope.edited_record = JSON.parse(JSON.stringify(rec));
        if ($scope.edited_record.hasOwnProperty('ttl') == false) {
            $scope.edited_record.ttl = 3600;
        }
        $scope.edited_record.replace = replace;
        $scope.shown_forms[rt] = true;
        
        scroll_to('add_' + rt + '_form', 300);
    };

    $scope.save_record = function(rt) {
        $scope.edited_record.rdtype = rt;

        $http.post('/zones/' + $scope.zone_name + '/records', $scope.edited_record)
            .success(function(data, status, headers, config) {
                $scope.scroll_return = $('html').scrollTop();
                $scope.hide_forms();
                $scope.load_records();
            })
            .error(function(data, status, headers, config) {
                $scope.form_errors = data;
            });
    };

    $scope.cancel_record = function(rt) {
        $scope.shown_forms[rt] = false;
    };

    $scope.delete_record = function(rt, rec, recidx, really, $event) {
        $event.stopPropagation();

        // When the delete X icon is clicked, show the large delete button
        if (! really) {
            $scope.shown_delete_confirm[rt + '_' + recidx] = true;
            var td_id = 'edit_controls_' + rt + '_' + recidx;
            $('#' + td_id).addClass('red_bg');
        }
        // When the TD is clicked, if it is currently a large delete button,
        // really delete the record.
        else {
            if ($scope.shown_delete_confirm[rt + '_' + recidx] != true) { return; }
            $http({
                    'url': '/zones/' + $scope.zone_name + '/records',
                    'method': 'DELETE',
                    'data': rec,
                    'headers': { 'Content-type': 'application/json' }
                })
                .success(function(data, status, headers, config) {
                    $scope.scroll_return = $('html').scrollTop();
                    $scope.hide_forms();
                    $scope.load_records();
                })
                .error(function(data, status, headers, config) {
                    $scope.hide_forms();
                });
        }
    };

    $scope.hide_forms();
    $scope.load_records();
}