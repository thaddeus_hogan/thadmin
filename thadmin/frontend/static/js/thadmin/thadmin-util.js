/********* UTILITY FUNCTIONS *********/
function scroll_to(element_id, speed) {
    setTimeout(function() {
        var element = $('#' + element_id);
        var scrollTop = $('html').scrollTop();
        var scrollPosMax = element.offset().top;
        var scrollPos = element.offset().top - $('html').height()
            + element.height() + $('footer').height();

        if (scrollTop > scrollPosMax || scrollTop < scrollPos) {
            var scroll_element = $("html, body");

            scroll_element.animate({
                scrollTop: scrollPos + 2
            }, speed);
        }
    }, 0);
}

function scroll_set(pos) {
    var scroll_element = $("html, body");
    scroll_element.scrollTop(pos);
}

function set_loading($scope, state, element_id, var_name) {
    if (! element_id) { element_id = 'load_spin'; }
    if (! var_name) { var_name = 'loading'; }

    if (state == true) {
        $scope[var_name] = true;
        var spinner_box = document.getElementById(element_id);
        $scope[element_id] = new Spinner(spinner_config).spin(spinner_box);
    } else {
        $scope[element_id].stop();
        $scope[var_name] = false;
    }
}

function set_saving($scope, state) {
    if (state == true) {
        $scope.saving = true;
        var spinner_box = document.getElementById('save_spin');
        $scope.save_spin = new Spinner(spinner_config).spin(spinner_box);
    } else {
        $scope.save_spin.stop();
        $scope.saving = false;
    }
}