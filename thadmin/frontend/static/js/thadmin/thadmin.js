// Spinner setup
var spinner_config = {
    lines: 13, // The number of lines to draw
    length: 6, // The length of each line
    width: 4, // The line thickness
    radius: 5, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#000', // #rgb or #rrggbb
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: 'auto', // Top position relative to parent in px
    left: 'auto' // Left position relative to parent in px
};

// IndexController here becuase it does nothing
function StaticController($scope) {}

// Main application module
var thadmin = angular.module('thadmin', ['ngCookies']);

// Configure URLs
thadmin.config(function($routeProvider) {
    $routeProvider.
    when('/index', { templateUrl: '/static/ngt/index.html', controller: StaticController }).
    when('/login', { templateUrl: '/static/ngt/login.html', controller: AuthController }).
    when('/zoneman', { redirectTo: '/zoneman/zonelist' }).
    when('/zoneman/zonelist', { templateUrl: '/static/ngt/zoneman/zonelist.html', controller: ZoneListController }).
    when('/zoneman/zoneadd', { templateUrl: '/static/ngt/zoneman/zoneadd.html', controller: ZoneAddController }).
    when('/zoneman/zone/:zone_name', { templateUrl: '/static/ngt/zoneman/records.html', controller: ZoneRecordsController }).
    when('/accounts/services', { templateUrl: '/static/ngt/accounts/servicelist.html', controller: ServiceListController }).
    when('/accounts/services/:service_id', { templateUrl: '/static/ngt/accounts/serviceedit.html', controller: ServiceEditController }).
    when('/accounts/accounts', { templateUrl: '/static/ngt/accounts/accountlist.html', controller: AccountsListController }).
    when('/accounts/invitations', { templateUrl: '/static/ngt/accounts/invitationlist.html', controller: InvitationListController }).
    when('/accounts/signup', { templateUrl: '/static/ngt/accounts/signup.html', controller: StaticController }).
    when('/accounts/redeem_invite', { templateUrl: '/static/ngt/accounts/redeem_invite.html', controller: RedeemInviteController }).
    when('/accounts/create_account', { templateUrl: '/static/ngt/accounts/create_account.html', controller: CreateAccountController }).
    when('/accounts/account_created', { templateUrl: '/static/ngt/accounts/account_created.html', controller: PostCreateAccountController }).
    when('/accounts/welcome', { templateUrl: '/static/ngt/accounts/welcome.html', controller: StaticController }).
    when('/accounts/set_password', { templateUrl: '/static/ngt/accounts/set_password.html', controller: SetPasswordController }).
    when('/accounts/current_order', { templateUrl: '/static/ngt/accounts/current_order.html', controller: CurrentOrderController }).
    when('/accounts/orders/:order_id', { templateUrl: '/static/ngt/accounts/order_detail.html', controller: OrderDetailController }).
    when('/accounts/:account_id/summary', { templateUrl: '/static/ngt/accounts/account_summary.html', controller: AccountSummaryController }).
    otherwise({ redirectTo: '/index' });
});

thadmin.run(function($rootScope, $http, $cookies, $location) {
    // Initial Authentication State
    var anon_userinfo = {
        email: '',
        is_superuser: false
    };

    $rootScope.authenticated = false;
    $rootScope.userinfo = anon_userinfo;
    // Setup AngularJS to send Django CSRF token with AJAX calls
    $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];

    // Add root scope logout handler
    $rootScope.do_logout = function() {
        $http.post('/common/auth/logout')
            .success(function(data, status, headers, config) {
                $rootScope.authenticated = false;
                $rootScope.userinfo = anon_userinfo;
                $location.path('/index');
            })
            .error(function(data, status, headers, config) {
                $rootScope.get_userinfo;
            });
    };

    // Add root scope handler to refresh UI authentication state
    $rootScope.get_userinfo = function() {
        $http.get('/common/auth/userinfo')
            .success(function(data, status, headers, config) {
                $rootScope.authenticated = true;
                $rootScope.userinfo = data;
            })
            .error(function(data, status, headers, config) {
                $rootScope.authenticated = false;
                $rootScope.userinfo = anon_userinfo;
            });
    };

    // FORMATTING FUNCTIONS
    $rootScope.f_datetime = function(datetime) {
        if (! datetime || datetime == null || datetime.length == 0) {
            return '';
        }
        
        var mmt = moment(datetime);
        return mmt.format('YYYY-MM-DD hh:mm A');
    };

    // Get actual authentication state and update UI
    $rootScope.get_userinfo();

    // Load any order in progress from local storage
    if (localStorage.current_order) {
        $rootScope.current_order = JSON.parse(localStorage.current_order);
    } else {
        $rootScope.current_order = [];
    }

    /********* CONSTANTS *********/
    $rootScope.service_codes = {
        1: 'Domain Name',
        2: 'Mailbox',
        3: 'Django Site 20/100',
        4: 'Mezzanine Site 20/100'
    };

    $rootScope.sc_domain_name = 1;
    $rootScope.sc_mailbox = 2;
    $rootScope.sc_django_site_20_100 = 3;
    $rootScope.sc_mez_site_20_100 = 4;

    $rootScope.process_states = {
        0: 'Pending',
        1: 'Running',
        2: 'Failed',
        3: 'Complete'
    };

    $rootScope.ps_pending = 0;
    $rootScope.ps_running = 1;
    $rootScope.ps_failed = 2;
    $rootScope.ps_complete = 3;

    $rootScope.order_states = {
        0: 'New',
        1: 'Paid',
        2: 'Fulfilled',
        3: 'Cancelled'
    };

    $rootScope.os_new = 0;
    $rootScope.os_paid = 1;
    $rootScope.os_fulfilled = 2;
    $rootScope.os_cancelled = 3;

    $rootScope.service_credit_state = {
        0: 'Available',
        1: 'In Use',
        2: 'Consumed'
    };

    $rootScope.scs_available = 0;
    $rootScope.scs_inuse = 1;
    $rootScope.scs_consumed = 2;
});