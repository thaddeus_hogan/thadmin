from django.shortcuts import render
from django.core.context_processors import csrf

from thadmin import settings

def thadmin(request):
    context = {
        'STRIPE_KEY_PUBLIC': settings.STRIPE_KEY_PUBLIC
    }
    context.update(csrf(request))
    return render(request, 'thadmin.html', context)