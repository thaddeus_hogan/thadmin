import dns.query
import dns.update
import dns.zone
import dns.tsig
import dns.tsigkeyring
from dns import rdatatype
from dns.name import LabelTooLong

from django.core.exceptions import PermissionDenied

from thadmin.zoneman import models
from thadmin.accounts.services import account_id_for_manager, submit_process

class ValidationError(Exception):
    def __init__(self, message, errors):
        Exception.__init__(self, message)
        self.errors = errors

def is_zone_manager(zone_name, email, zone = None):
    try:
        account_id = account_id_for_manager(email)
        if zone is None:
            zone = models.Zone.objects.get(zone_name = zone_name)    
        return account_id == zone.account_id
    except:
        return False

    return True

def get_zone(zone_name):
    return models.Zone.objects.get(zone_name = zone_name)

def get_dns_config(zone_name):
    zone = get_zone(zone_name)
    keyring = dns.tsigkeyring.from_text({
        zone.tsig_key_name: zone.tsig_key
    })

    dns_config = {
        'keyring': keyring,
        'keyname': zone.tsig_key_name,
        'keyalgorithm': getattr(dns.tsig, zone.tsig_key_algo),
        'nameserver': zone.nameserver,
    }

    return dns_config

def validate_record(rec):
    errors = {}

    # Test that records exist
    if 'name' not in rec or len(rec['name']) < 1:
        errors['name'] = "Name must be specified"
    if 'ttl' not in rec:
        errors['ttl'] = "TTL must be specified"
    if 'value' not in rec or len(rec['name']) < 1:
        errors['value'] = "Value must be specified"
    if 'rdtype' not in rec:
        errors['rdtype'] = "'rdtype' of record must be specified, text format"

    # Default replace behavior
    if 'replace' not in rec:
        rec['replace'] = False

    # Test TTL validity
    try:
        rec['ttl'] = int(rec['ttl'])
    except ValueError:
        errors['ttl'] = "TTL must be an integer"

    if rec['ttl'] < 300 or rec['ttl'] > 86400:
        errors['ttl'] = "TTL must be between 300 and 86400 inclusive"

    rdtype = rec['rdtype'].upper();

    # Validate A record
    if rdtype == 'A':
        if re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', rec['value']) is None:
            errors['value'] = "Value not an IPv4 address"

    # Validate MX Records
    if rdtype == 'MX':
        mx_parts = rec['value'].split(' ')
        if len(mx_parts) != 2:
            errors['value'] = "MX record must have priority and host separated by a space"
        try:
            mx_priority = int(mx_parts[0])
            if mx_priority < 10 or mx_priority > 1000:
                errors['value'] = "MX record priority must be between 10 and 1000 inclusive"
        except ValueError:
            errors['value'] = "MX record priority part must be an integer"

    return errors

def zone_list(account_id = None):
    if account_id is None:
        return models.Zone.objects.all()
    else:
        return models.Zone.objects.filter(account_id = account_id)

def create_zone(account_id, zone_name):
    submit_process(account_id, "Create DNS Zone " + zone_name)

def zone_records(zone_name):
    dns_config = get_dns_config(zone_name)

    xfr = dns.query.xfr(dns_config['nameserver'], zone_name,
        keyring = dns_config['keyring'], keyname = dns_config['keyname'],
        keyalgorithm = dns_config['keyalgorithm'])
    zone = dns.zone.from_xfr(xfr)

    names = zone.nodes.keys()
    records = { 'A': [], 'CNAME': [], 'MX': [], 'SRV': [], 'TXT': [], 'SPF': [] }

    for n in names:
        for rd in zone[n]:
            for i in rd.items:
                rec = {
                    'name': unicode(n),
                    'ttl': rd.ttl,
                    'rdtype': rdatatype.to_text(rd.rdtype),
                }

                if rd.rdtype == rdatatype.A:
                    rec['value'] = unicode(i.address)
                    records['A'].append(rec)
                elif rd.rdtype == rdatatype.CNAME:
                    rec['value'] = unicode(i.target)
                    records['CNAME'].append(rec)
                elif rd.rdtype == rdatatype.MX:
                    rec['value'] = "%s %s" % (unicode(i.preference), unicode(i.exchange))
                    records['MX'].append(rec)
                elif rd.rdtype == rdatatype.SRV:
                    rec['value'] = "%s %s %s %s" % (unicode(i.priority), unicode(i.weight),
                        unicode(i.port), unicode(i.target))
                    records['SRV'].append(rec)
                elif rd.rdtype == rdatatype.TXT:
                    rec['value'] = unicode(i.strings[0])
                    records['TXT'].append(rec)
                elif rd.rdtype == rdatatype.SPF:
                    rec['value'] = unicode(i.strings[0])
                    records['SPF'].append(rec)

    return records

def write_record(zone_name, rec):
    dns_config = get_dns_config(zone_name)
    update = dns.update.Update(zone_name, keyring = dns_config['keyring'],
        keyname = dns_config['keyname'], keyalgorithm = dns_config['keyalgorithm'])

    errors = validate_record(rec)
    if len(errors) > 0:
        raise ValidationError("DNS record failed validation", errors)

    rdtype = rec['rdtype'].upper();
    if rdtype == 'TXT' or rdtype == 'SPF':
        rec['value'] = '"' + rec['value'] + '"'
    
    # PREPARE the update
    try:
        if rec['replace'] == True:
            update.replace(rec['name'].encode('ascii', 'ignore'), rec['ttl'],
                rec['rdtype'].encode('ascii', 'ignore'), rec['value'].encode('ascii', 'ignore'))
        else:
            update.add(rec['name'].encode('ascii', 'ignore'), rec['ttl'],
                rec['rdtype'].encode('ascii', 'ignore'), rec['value'].encode('ascii', 'ignore'))
    except LabelTooLong:
        errors['name'] = "Name is too long, name + domain name may be at most 255 characters"
        raise ValidationError("DNS record failed validation", errors)
    except:
        errors['value'] = "DNS server failed to parse this update"
        raise ValidationError("DNS record failed validation", errors)

    # EXECUTE the update
    response = dns.query.tcp(update, dns_config['nameserver'])

    if response.rcode() == 5:
        errors['name'] = "DNS Server refused this update"
        raise ValidationError("DNS record failed validation", errors)

    # Error Notes (tested rcode responses):
    #
    # MX points to name that doesn't exist - 5 - Refused
    # MX record missing priority - dnspython throws SyntaxError
    # Completely duplicate record - No error
    # Long name - dnspython throws LabelTooLong
    # Invalid characters - 5 - Refused

def delete_record(zone_name, rec):
    dns_config = get_dns_config(zone_name, request.user)
    update = dns.update.Update(zone_name, keyring = dns_config['keyring'],
        keyname = dns_config['keyname'], keyalgorithm = dns_config['keyalgorithm'])

    rdtype = rec['rdtype'].upper();
    if rdtype == 'TXT' or rdtype == 'SPF':
        rec['value'] = '"' + rec['value'] + '"'

    errors = validate_record(rec)
    if len(errors) > 0:
        raise ValidationError("DNS record failed validation", errors)

    update.delete(rec['name'].encode('ascii', 'ignore'),
        rec['rdtype'].encode('ascii', 'ignore'), rec['value'].encode('ascii', 'ignore'))
    
    response = dns.query.tcp(update, dns_config['nameserver'])