import re

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from django.http import Http404
from django.core.exceptions import PermissionDenied

import dns.query
import dns.update
import dns.zone
import dns.tsig
import dns.tsigkeyring
from dns import rdatatype
from dns.name import LabelTooLong

from thadmin import no_cache_headers
from thadmin.zoneman import models, services
from thadmin.accounts.services import account_id_for_manager

class ZoneList(APIView):
    def get(self, request, format=None):
        if request.user.is_superuser:
            zones = services.zone_list()
        else:
            account_id = account_id_for_manager(request.user.username)
            zones = services.zone_list(account_id = account_id)

        return Response(zones)

class ZoneRecordList(APIView):
    def get(self, request, zone_name, format = None):
        if request.user.is_superuser == False:
            if services.is_zone_manager(zone_name, request.user.username) == False:
                return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        records = services.zone_records(zone_name)
        return Response(records, headers = no_cache_headers)

    def post(self, request, zone_name, format = None):
        if request.user.is_superuser == False:
            if services.is_zone_manager(zone_name, request.user.username) == False:
                return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        try:
            services.write_record(zone_name, request.DATA)
        except ValidationError, ve:
            return Response(ve.errors, status.HTTP_400_BAD_REQUEST)

        return Response('', status.HTTP_200_OK)

    def delete(self, request, zone_name, format = None):
        if request.user.is_superuser == False:
            if services.is_zone_manager(zone_name, request.user.username) == False:
                return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        try:
            services.delete_record(zone_name, request.DATA)
        except ValidationError, ve:
            return Response(ve.errors, status.HTTP_400_BAD_REQUEST)

        return Response('', status.HTTP_200_OK)