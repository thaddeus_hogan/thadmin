from django.db import models
from rest_framework import serializers

class Zone(models.Model):
    zone_name = models.CharField(max_length = 255, primary_key = True)
    account_id = models.CharField(max_length = 255, db_index = True)
    service_credit_id = models.CharField(max_length = 36)
    nameserver = models.CharField(max_length = 45)
    tsig_key = models.CharField(max_length = 100)
    tsig_key_name = models.CharField(max_length = 255)
    tsig_key_algo = models.CharField(max_length = 25)

class ZoneSzr(serializers.ModelSerializer):
    class Meta:
        model = Zone
        fields = ('zone_name', 'account_id', 'service_credit_id')