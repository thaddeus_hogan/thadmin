from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^api-auth/', include('rest_framework.urls', namespace = 'rest_framework')),
    url(r'^common/', include('thadmin.common.urls')),
    url(r'^zones/', include('thadmin.zoneman.urls')),
    url(r'^accounts/', include('thadmin.accounts.urls')),
    url(r'^thadmin/$', 'thadmin.frontend.views.thadmin'),
)
