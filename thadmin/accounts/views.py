from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions

from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.db.models import Q

import stripe

from thadmin import no_cache_headers
from thadmin.accounts import models, services

class ServiceList(APIView):
    def get(self, request, format = None):
        service_list = models.HostingService.objects.filter(active = True)
        szr = models.HostingServiceSzr(service_list, many = True)
        return Response(szr.data, headers = no_cache_headers)

    def post(self, request, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        szr = models.HostingServiceSzr(data = request.DATA)
        if szr.is_valid():
            service = szr.save()
            return Response(service.id, status = status.HTTP_201_CREATED)
        return Response(szr.errors, status = status.HTTP_400_BAD_REQUEST)

class Service(APIView):
    def get(self, request, id, format = None):
        try:
            service = models.HostingService.objects.get(id = id)
        except:
            return Response('No such service', status.HTTP_404_NOT_FOUND)

        szr = models.HostingServiceSzr(service)
        return Response(szr.data, headers = no_cache_headers)

    def put(self, request, id, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        try:
            service = models.HostingService.objects.get(id = id)
        except:
            return Response('No such service', status.HTTP_404_NOT_FOUND)

        szr = models.HostingServiceSzr(service, data = request.DATA)
        if szr.is_valid():
            szr.save()
            return Response(status = status.HTTP_204_NO_CONTENT)
        return Response(szr.errors, status = status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        try:
            service = models.HostingService.objects.get(id = id)
        except:
            return Response('No such service', status.HTTP_404_NOT_FOUND)

        service.active = False
        service.save()

        return Response(status = status.HTTP_204_NO_CONTENT)

class AccountList(APIView):
    def get(self, request, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        accounts = models.Account.objects.all()
        szr = models.AccountSzr(accounts, many = True)
        return Response(szr.data, headers = no_cache_headers)

class Account(APIView):
    def get(self, request, account_id, format = None):
        account = models.Account.objects.get(account_id = account_id)

        if services.account_id_for_manager(request.user.username) != account_id:
            if request.user.is_superuser == False:
                return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        szr = models.AccountSzr(account)
        return Response(szr.data, headers = no_cache_headers)

class AccountSummary(APIView):
    def get(self, request, account_id, format = None):
        account = models.Account.objects.get(account_id = account_id)

        if request.user.is_superuser == False:
            if services.account_id_for_manager(request.user.username) != account_id:
                return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        account_szr = models.AccountSzr(account)
        summary = account_szr.data

        # Available Service Credits
        sc_available = models.ServiceCredit.objects.filter(
            account_id = account_id, credit_state = models.scs_available
            ).order_by('expires_datetime')
        sc_available_szr = models.ServiceCreditSzr(sc_available, many = True)
        summary['sc_available'] = sc_available_szr.data

        # In Use Service Credits
        sc_inuse = models.ServiceCredit.objects.filter(
            account_id = account_id, credit_state = models.scs_inuse
            ).order_by('expires_datetime')
        sc_inuse_szr = models.ServiceCreditSzr(sc_inuse, many = True)
        summary['sc_inuse'] = sc_inuse_szr.data

        # Active Processes
        active_processes = models.AccountProcess.objects.filter(
            Q(account_id = account_id) & Q(
                Q(process_state = models.ps_pending) | Q(process_state = models.ps_running)
            )).order_by('-submit_datetime')
        active_processes_szr = models.AccountProcessSzr(active_processes, many = True)
        summary['active_processes'] = active_processes_szr.data

        # Recent Orders
        recent_orders = models.Order.objects.filter(
            account_id = account_id).order_by('-submit_datetime')[:3]
        recent_orders_szr = models.OrderSzr(recent_orders, many = True)
        summary['recent_orders'] = recent_orders_szr.data

        return Response(summary, headers = no_cache_headers)

class AccountCreate(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format = None):
        if request.session['invited'] != True:
            return Response({ 'account', 'No record of redeemed invitation'},
                status.HTTP_403_FORBIDDEN)

        errors = services.clean_account_data(request.DATA)
        if len(errors) > 0:
            return Response(errors, status.HTTP_400_BAD_REQUEST)

        del request.session['invited']

        account_id = services.create_account(request.DATA)

        request.session['new_account_mail'] = account_id + '@thogan.com'
        services.set_pwrs_token(request.session, 1800)

        return Response(account_id, status.HTTP_201_CREATED)

class InvitationList(APIView):
    def get(self, request, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        invites = models.Invitation.objects.all()
        szr = models.InvitationSzr(invites, many = True)
        return Response(szr.data, headers = no_cache_headers)

    def post(self, request, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        invite_data = request.DATA
        if not invite_data.has_key('issued_to_mail') or not invite_data.has_key('issued_to_name'):
            return Response('Entity must include fields issued_to_mail and issued_to_name',
                status = status.HTTP_400_BAD_REQUEST)

        invite_code = services.create_invitation(
            invite_data['issued_to_mail'], invite_data['issued_to_name'])

        return Response(invite_code, status = status.HTTP_201_CREATED)

class Invitation(APIView):
    def get(self, request, invite_code, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        invite = models.Invitation.objects.get(invite_code = invite_code)
        szr = models.InvitationSzr(invite)
        return Response(szr.data, headers = no_cache_headers)

    def delete(self, request, invite_code, format = None):
        if request.user.is_superuser == False:
            return Response('Access Denied', status.HTTP_403_FORBIDDEN)

        invite = models.Invitation.objects.get(invite_code = invite_code)
        invite.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class InvitationRedeem(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, invite_code, format = None):
        if services.redeem_invitation(invite_code):
            request.session['invited'] = True
            return Response(status = status.HTTP_204_NO_CONTENT)
        return Response("Invitation code invalid", status.HTTP_403_FORBIDDEN)

class PasswordValidator(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format = None):
        result = services.validate_password(request.DATA)
        return Response(result)

class PasswordSet(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format = None):
        if 'pwrs_token' not in request.session:
            return Response("Unauthorized", status = status.HTTP_403_FORBIDDEN)

        if not services.validate_pwrs_token(request.session):
            return Response("Password reset token expired", status = status.HTTP_403_FORBIDDEN)

        try:
            if 'new_account_mail' in request.session:
                mail = request.session['new_account_mail']
                del request.session['new_account_mail']
                services.set_password(mail, request.DATA)
            else:
                services.set_password(request.user.username, request.DATA)
        except services.PasswordSetFailed as e:
            return Response(str(e), status = status.HTTP_400_BAD_REQUEST)

        return Response(status = status.HTTP_204_NO_CONTENT)

class OrderList(APIView):
    def post(self, request, format = None):
        try:
            order = services.create_order(request.user.username, request.DATA)
        except services.CreateOrderError as e:
            return Response(str(e), status = status.HTTP_400_BAD_REQUEST)

        return Response(services.serialize_order(order), status = status.HTTP_201_CREATED)

class Order(APIView):
    def get(self, request, order_id, format = None):
        order = models.Order.objects.get(id = order_id)
        if request.user.is_superuser == False:
            if order.account_id != services.account_id_for_manager(request.user.username):
                return Response("Not authorized to view this order", status = status.HTTP_403_FORBIDDEN)
        return Response(services.serialize_order(order), headers = no_cache_headers)

class OrderPay(APIView):
    def post(self, request, order_id, format = None):
        try:
            services.process_payment(order_id, request.DATA)
        except stripe.CardError as ce:
            return Response(ce.json_body['error'], status = status.HTTP_400_BAD_REQUEST)
        except services.PaymentDataInvalid as pdi:
            error = {
                'code': 'missing_field',
                'field': pdi.field
            }
            return Response(error, status = status.HTTP_400_BAD_REQUEST)
        return Response(status = status.HTTP_204_NO_CONTENT)
