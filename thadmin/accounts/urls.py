from django.conf.urls import patterns, include, url
from thadmin.accounts import views

urlpatterns = patterns('',
    url(r'^services/$', views.ServiceList.as_view()),
    url(r'^services/(?P<id>\d+)$', views.Service.as_view()),
    url(r'^accounts/$', views.AccountList.as_view()),
    url(r'^accounts/create$', views.AccountCreate.as_view()),
    url(r'^accounts/(?P<account_id>hs-[0-9a-zA-Z\-]+)$', views.Account.as_view()),
    url(r'^accounts/(?P<account_id>hs-[0-9a-zA-Z\-]+)/summary$',
        views.AccountSummary.as_view()),
    url(r'^invitations/$', views.InvitationList.as_view()),
    url(r'^invitations/(?P<invite_code>[a-zA-Z0-9-]{36})$',
        views.Invitation.as_view()),
    url(r'^invitations/(?P<invite_code>[a-zA-Z0-9-]{36})/redeem$',
        views.InvitationRedeem.as_view()),
    url(r'^password/validate$', views.PasswordValidator.as_view()),
    url(r'^password/set$', views.PasswordSet.as_view()),
    url(r'^orders/$', views.OrderList.as_view()),
    url(r'^orders/(?P<order_id>\d+)$', views.Order.as_view()),
    url(r'^orders/(?P<order_id>\d+)/pay$', views.OrderPay.as_view()),
)