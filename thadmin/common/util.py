import ldap
import ldap.filter
from thadmin import settings

class MailNotUnique(Exception):
    pass

def ldap_connect():
    ldap_conn = ldap.initialize(settings.LDAP_MGR_URI)
    ldap_conn.simple_bind_s(who = settings.LDAP_MGR_BIND_DN,
        cred = settings.LDAP_MGR_BIND_PASS)
    return ldap_conn

def ldap_close(ldap_conn):
    ldap_conn.unbind()

def dn_for_mail(ldap_conn, mail):
    mail_clean = ldap.filter.escape_filter_chars(mail)
    res = ldap_conn.search_s(settings.LDAP_OU_USERS, ldap.SCOPE_SUBTREE,
        "(&(objectClass=inetOrgPerson)(mail=%s))" % mail_clean)
    
    if len(res) > 1:
        raise MailNotUnique("Will not change password if duplicate mails exist in directory.")
    elif len(res) == 0:
        return None

    return res[0][0]