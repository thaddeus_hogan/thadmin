from rest_framework import serializers
from django.forms import widgets

class MailAndPass(serializers.Serializer):
    email = serializers.CharField(required = True, max_length = 254)
    password = serializers.CharField(required = True, max_length = 1024, widget = widgets.PasswordInput)

    def restore_object(self, attrs, instance = None):
        instance = {}
        instance['email'] = attrs.get('email')
        instance['password'] = attrs.get('password')

        return instance
